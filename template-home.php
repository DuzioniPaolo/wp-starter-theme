<?php if(__FILE__ == $_SERVER['SCRIPT_FILENAME']){ die(); }
/**
 * Template Name: Homepage
 */
?>
<?php get_header(); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">

		<h1><?php the_title(); ?></h1>

		<?php if ( have_posts() ) :
		  while ( have_posts() ) : the_post(); ?>

			<div class="testo">
				<?php the_content(); ?>
			</div>

		  <?php endwhile; ?>
		<?php endif; ?>

	</div>
</div>


<?php get_footer(); ?>
