<div class="row widget">
    <form class="col-md-12" action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
      <div class="row collapse">
        <div class="col-md-3">
          <button type="submit" value="" class="button postfix" id="searchsubmit" ></button>
        </div>
        <div class="col-md-9">
          <input type="text" id="s" name="s" value="" placeholder="Search for:"/>
        </div>
      </div>
    </form>
  </div>
