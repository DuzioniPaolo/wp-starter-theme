<?php if(__FILE__ == $_SERVER['SCRIPT_FILENAME']){ die(); } get_header(); global $get_the_opt; ?>

<div class="container">
	<div class="row">

		<section id="pagenotfound" class="col-md-12 text-center">
			<p><i class="fa fa-frown-o fa-4x"></i></p>
			<p><strong><?php _e("Ooops ... there's a problem!", 'beprime'); ?></strong></p>
			<p><?php _e("Something is wrong! Are you really sure that what you're looking for is on this site?", 'beprime'); ?></p>
		</section>

	</div>
</div>

<?php get_footer(); ?>
