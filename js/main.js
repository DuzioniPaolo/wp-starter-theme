// JQuery No Conflict
(function($){



$(document).ready(function() {

	/**
	 * BOOTSTRAP MAIN NAV DROPDOWN-MENU WITH PARENT CLICKABLE
	 * (2 clicks on mobile device: first open the dropdown-menu, second click the parent link)
	 *
	 * BASED ON:
	 * - WP Bootstrap Navwalker (https://github.com/twittem/wp-bootstrap-navwalker)
	 		 In core -> Choose_config -> wp_bootstrap_navwalker.php (Edited by Paolo Duzioni)
	 * - WP Eden for the original jQuery code (http://wpeden.com/)
	 *
	 * By Paolo Duzioni
	 * Vers. 1.0.0
	 */

	//Refs
	var dropdowns = $('.navbar .menu-item-has-children'),
			allDropdownMenus = dropdowns.find('.dropdown-menu'),
			parentLink = dropdowns.children('a');

	// Desktop and up
	if($(window).width() > 1024){
		dropdowns.hover(
			function() {
				$(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
			},
			function() {
				$(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();
		});
		//Click on parent link
		parentLink.click(function(){
			location.href = this.href;
		});
	}
	//Tablet and down
	else{
		parentLink.click(function(){

			var dropdownMenu = $(this).next('.dropdown-menu');

			//the second click on parent
			if(dropdownMenu.hasClass('drop-active')){
				dropdownMenu.removeClass('drop-active');
			 	location.href = this.href;
			}
			//the first click on parent
			else{
				dropdownMenu.addClass('drop-active');
			}

			//close and reset all previous expanded dropdown-menus
			allDropdownMenus.not(dropdownMenu).removeClass('drop-active');
		});
	}



	/**
	 * INIT SLIDER PRO INSTANCE
	 */
	$( '#my-slider' ).sliderPro({
		width: '100%',
		//height: 560,
		autoHeight: false,
		arrows: true,
		buttons: false,
		fade: false,
		loop: true,
		autoplay: true,
		slideDistance:0
	});



	/**
	 * INIT TOSRUS LIGHTBOX INSTANCE
	 */
	$('a.tos-gal').tosrus({
		infinite:true,
		pagination:{
			add:true,
			type:"thumbnails"
		},
		keys:{
			next: true,
			prev: true,
			close: true
		}
	});



// End Doc Ready
});

// End jQuery No Conflict
})(jQuery);
