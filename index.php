<?php get_header(); ?>
<div class="container">
<div class="row">

<div class="col-md-3">
	<?php get_sidebar(); ?>
</div>

<div class="col-md-9 posts_wrp">
		<?php //choose_breadcrumbs(); ?>
	<?php if (have_posts()) :
       while (have_posts()) : the_post(); ?>

		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
				<div class="meta panel">
					<div class="row">
					<div class="large-10 columns">
					<h6 class="subheader"><em> <i class="fa fa-calendar"></i> </em> <?php the_time('F jS, Y') ?> <em> <i class="fa fa-user"></i></em> <?php the_author() ?></h6>
					</div>
					<div class="large-2 columns">
					<a href="#" class="button right split tiny comments_btn"><i class="fa fa-comments"></i> <span data-dropdown="drop<?php echo $number ?>"></span></a><br>
					<ul id="drop<?php echo $number ?>" class="f-dropdown" data-dropdown-content>
							<li><p><?php comments_number( 'no responses', 'one response', '% responses' ); ?></p></li>
					</ul>
					</div>
					</div>
				</div>
			</header>


			<?php if ( has_post_thumbnail() ) : ?>
				<?php the_post_thumbnail(); ?>
			<?php endif; ?>

			<div class="entry">
				<?php the_excerpt(); ?>
			</div>

			<footer>
				<div>
						<i class="fa fa-folder-open-o"></i> <?php the_category(', '); ?>
				</div>
			</footer>
		</article>

	<?php endwhile; ?>

	<?php else : ?>

		<div data-alert class="alert-box secondary">
		 	<p><?php echo __( 'No posts found.', 'chooseWp_framework' ); ?></p>
		  <a href="#" class="close"><i class="fa fa-search"></i></a>
		</div>

	<?php endif; ?>
</div>

</div>
</div>
<?php get_footer(); ?>
