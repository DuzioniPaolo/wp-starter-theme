<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
	<?php global $choose_wp; ?>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" />
	<?php } ?>

	<title><?php bloginfo('name'); ?></title>

	<link rel="shortcut icon" href="<?php echo $choose_wp['ch_top_head_favicion']['url']; ?>" type="image/x-icon" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

	<?php wp_head(); ?>
</head>

  

<body <?php body_class(); ?>>

<header>
	<nav class="navbar navbar-default">
	  <div class="container">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="<?php echo esc_url(home_url()); ?>">
					<?php $isActive = $choose_wp['ch_top_head_logo_toggle'];

					if ( ($isActive == "2" )) : //Immagine logo impostata ?>
						<img alt="<?php bloginfo('name'); ?>" src="<?php echo $choose_wp['ch_top_head_logo']['url']; ?>">
					<?php else : ?>
						<?php bloginfo('name'); ?>
					<?php endif; ?>
				</a>
	    </div>

	    <!-- Collect the nav links and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<?php
				$args = array(
					'menu' => 'main-menu',
					'menu_class' => 'nav navbar-nav navbar-right',
					'container' => '',
					'theme_location' => 'main-menu',
					'fallback_cb' => 'wp_page_menu',
					//nav walker for bootstrap classes
          'walker' => new wp_bootstrap_navwalker()
				);
				wp_nav_menu($args); ?>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container -->
	</nav>
</header>




<?php if ( is_front_page() ) : //EVENTUALE SLIDER  IN HOMEPAGE ?>

	<?php if( have_rows('slider_site') ): ?>
		<div class="container">
			<div class="col-md-8 col-md-offset-2">
				<div class="slider-pro" id="my-slider">
					<div class="sp-slides">
						<?php while( have_rows('slider_site') ): the_row();

							$image = get_sub_field('image'); ?>

							<div class="sp-slide">
								<img class="sp-image" src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>"  width="<?php echo $image['width']; ?>" height="<?php echo $image['height']; ?>" />
							</div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>

<?php endif; ?>
