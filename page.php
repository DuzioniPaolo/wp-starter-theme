<?php get_header(); ?>
<div class="container">
<div class="row">
<div class="col-md-12">

<?php //choose_breadcrumbs(); ?>
<div class="posts_main">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="post" id="post-<?php the_ID(); ?>">

			<div class="entry">

				<h1><?php the_title(); ?></h1>

				<?php the_content(); ?>

				<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>

			</div>

			<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>

		</div>

		<?php endwhile; endif; ?>
</div><!-- End posts_main -->

</div>
</div>
</div>
<?php get_footer(); ?>
