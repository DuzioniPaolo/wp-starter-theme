<div id="sidebar">

    <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Sidebar Widgets')) : else : ?>

        <!-- All this stuff in here only shows up if you DON'T have any widgets active in this zone -->

    	<?php get_search_form(); ?>
    <div class="widget">
    	<h2 class="subheader">Archives</h2>
    	<ul class="side-nav">
    		<?php wp_get_archives('type=monthly'); ?>
    	</ul>
	</div>
	<div class="widget">
        <h2 class="subheader">Categories</h2>
        <ul class="side-nav">
    	   <?php wp_list_categories('show_count=0&title_li='); ?>
        </ul>
	</div>
  <div class="widget">  
    	<h2 class="subheader">Meta</h2>
    	<ul class="side-nav">
    		<?php wp_register(); ?>
    		<li><?php wp_loginout(); ?></li>
    		<li><a href="http://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress</a></li>
    		<?php wp_meta(); ?>
    	</ul>
  </div>

	<?php endif; ?>

</div>
