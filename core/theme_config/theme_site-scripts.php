<?php

/********************* MAIN STYLES AND JAVASCRIPT FILES ****************/

// Enqueue styles
function beprime_theme_styles() {
    wp_enqueue_style('Google_fonts', 'https://fonts.googleapis.com/css?family=Titillium+Web:400,700');
    wp_enqueue_style( 'font-awseome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css' );
    wp_enqueue_style( 'bootstrap-css',  get_template_directory_uri() .'/core/bootstrap-3.3.6/css/bootstrap.min.css' );
    wp_enqueue_style( 'tosru-css',  get_template_directory_uri() .'/css/jquery.tosrus.all.min.css' );
    wp_enqueue_style( 'sliderpro-css',  get_template_directory_uri() .'/css/slider-pro.min.css' );
    wp_enqueue_style( 'main-css',  get_template_directory_uri() .'/css/default.css' );
}
add_action( 'wp_enqueue_scripts', 'beprime_theme_styles' );


// Enqueue script
function beprime_theme_scripts() {

  //jQuery
  wp_enqueue_script('jquery');

  //Modernizer
  wp_enqueue_script('modernizer_js', get_template_directory_uri() . '/core/bootstrap-3.3.6/js/vendor/modernizr.js', array( 'jquery' ), null, true);

  //bootstrap
  wp_enqueue_script('bootstrap_JS', get_template_directory_uri() . '/core/bootstrap-3.3.6/js/bootstrap.min.js', array( 'jquery' ), null, true);

  //Slider pro
  wp_enqueue_script('sliderpro-js', get_template_directory_uri() . '/js/jquery.sliderPro.min.js', array( 'jquery' ), null, true);

  //Tosrus
  wp_enqueue_script('tosru_js', get_template_directory_uri() . '/js/jquery.tosrus.min.all.js', array( 'jquery' ), null, true);

  //Main.js
  wp_enqueue_script('main_js', get_template_directory_uri() . '/js/main.js', array(), null, true);

  //Creazione oggetto globale passaggio dati da PHP a JS (AJAX, traduzioni stringe, theme urls, etc...)
  // wp_localize_script( 'main_js', 'my_vars', array(
  //   'ajaxurl'   => admin_url( 'admin-ajax.php' ),
  //   'nonce'     => wp_create_nonce( 'take-posts-nonce' ),
  // ));
}
add_action('wp_enqueue_scripts', 'beprime_theme_scripts');   
