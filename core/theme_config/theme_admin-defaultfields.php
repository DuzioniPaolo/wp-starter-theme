<?php

//*********************** Header Settings ***********************//

Redux::setSection( $opt_name, array(
    'title' => __( 'Header & Navigation', 'chooseWp_framework' ),
    'id'    => 'ch_top_nav_set',
    'desc'  => __( '', 'chooseWp_framework' ),
    'icon'  => 'el el-cogs'
));

// -> Header Settings

Redux::setSection( $opt_name, array(
    'title'      => __( 'Header Settings', 'chooseWp_framework' ),
    'id'         => 'ch_top_head_set_1',
    'desc'       => __( 'Header Settings ', 'chooseWp_framework' ),
    'subsection' => true,
    'fields'     => array(
    	array(
            'id'       => 'ch_top_head_logo_toggle',
            'type'     => 'button_set',
            'title'    => __( 'Top Navigation Style', 'chooseWp_framework' ),
            'subtitle' => __( 'Choose the style of the top Main Menu', 'chooseWp_framework' ),
            'desc'     => __( '', 'chooseWp_framework' ),
            'options'  => array(
                '1' => 'Text ( Default wordpress text )',
                '2' => 'Image'
            ),
            'default'  => '1'
        ),

         array(
            'id'       => 'ch_top_head_logo',
            'type'     => 'media',
            'url'      => true,
            'title'    => __( 'Top logo', 'redux-framework-demo' ),
            'compiler' => 'true',
            //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
            'desc'     => __( 'Add top logo image.', 'redux-framework-demo' ),
            'subtitle' => __( 'Upload any media, best size (125x230)', 'redux-framework-demo' ),
            'default'  => array( 'url' => 'http://s.wordpress.org/style/images/codeispoetry.png' ),
            //'hint'      => array(
            //    'title'     => 'Hint Title',
            //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
            //)
        ),
        array(
            'id'       => 'ch_top_head_favicion',
            'type'     => 'media',
            'url'      => true,
            'title'    => __( 'Favicon', 'redux-framework-demo' ),
            'compiler' => 'true',
            //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
            'desc'     => __( 'Add favicon.', 'redux-framework-demo' ),
            'subtitle' => __( 'Add .ico file', 'redux-framework-demo' ),
            'default'  => array( 'url' => 'http://www.beprime.it/wp-content/uploads/2015/11/favicon.png' ),
            //'hint'      => array(
            //    'title'     => 'Hint Title',
            //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
            //)
        ),
    )
) );

// -> Top nav Settings

Redux::setSection( $opt_name, array(
    'title'      => __( 'Main Menu', 'chooseWp_framework' ),
    'id'         => 'ch_top_nav_set_1',
    'desc'       => __( 'Manin nav & Header Settings ', 'chooseWp_framework' ),
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'ch_top_nav_button_type',
            'type'     => 'button_set',
            'title'    => __( 'Top Navigation Style', 'chooseWp_framework' ),
            'subtitle' => __( 'Choose the style of the top Main Menu', 'chooseWp_framework' ),
            'desc'     => __( '', 'chooseWp_framework' ),
            'options'  => array(
                '1' => 'In Line',
                '2' => 'Out'
            ),
            'default'  => '2'
        ),
        array(
            'id'       => 'ch_top_nav_button_pos',
            'type'     => 'button_set',
            'title'    => __( 'Top Nav Position', 'chooseWp_framework' ),
            'subtitle' => __( 'Choose the position of the top Main Menu', 'chooseWp_framework' ),
            'desc'     => __( '', 'chooseWp_framework' ),
            'options'  => array(
                'left' => 'Left',
                'right' => 'Right'
            ),
            'default'  => '1'
        ),
        array(
            'id'       => 'ch_top_nav_button_fix',
            'type'     => 'button_set',
            'title'    => __( 'Top Nav Fixed Position', 'chooseWp_framework' ),
            'subtitle' => __( 'Set if you want that navigation is in fixed position', 'chooseWp_framework' ),
            'desc'     => __( 'So if, scroll the page, navigation is always on top', 'chooseWp_framework' ),
            'options'  => array(
                'fixed' => 'Fixed',
                '' => 'Scroll'
            ),
            'default'  => '2'
        ),
        array(
            'id'       => 'ch_top_nav_button_full',
            'type'     => 'button_set',
            'title'    => __( 'Top Nav Full width', 'chooseWp_framework' ),
            'subtitle' => __( 'Set if you want that navigation is full width', 'chooseWp_framework' ),
            'desc'     => __( '', 'chooseWp_framework' ),
            'options'  => array(
                'contain-to-grid' => 'Full Width',
                '' => 'Contain'
            ),
            'default'  => '2'
        ),

    )
) );



//*********************** SEO & Breadcrumbs Settings ***********************//

Redux::setSection( $opt_name, array(
    'title' => __( 'SEO & Breadcrumbs', 'chooseWp_framework' ),
    'id'    => 'ch_seo_set',
    'desc'  => __( '', 'chooseWp_framework' ),
    'icon'  => 'el-pencil'
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Sitemap XML', 'redux-framework-demo' ),
    'id'         => 'ch_seo_switch',
    'desc'       => __( 'For full documentation on this field, visit: ', 'redux-framework-demo' ) . '<a href="//docs.reduxframework.com/core/fields/switch/" target="_blank">docs.reduxframework.com/core/fields/switch/</a>',
    'subsection' => true,
    'fields'     => array(

        array(
            'id'       => 'ch_seo_xmlactive',
            'type'     => 'switch',
            'title'    => __( 'SEO - active XML settings', 'redux-framework-demo' ),
            'subtitle' => __( 'Look, it\'s on! Also hidden child elements!', 'redux-framework-demo' ),
            'default'  => 0,
            'on'       => 'Enabled',
            'off'      => 'Disabled',
        ),
        array(
            'id'       => 'ch_seo_xmlexclude',
            'type'     => 'select',
            'data' => 'post_type',
            'multi'    => true,
            'title'    => __( 'Include posts type', 'redux-framework-demo' ),
            'subtitle' => __( 'Select all posts type that will be insered in the xml sitemap', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => array( 'page' ),
        ),
    )
) );

Redux::setSection( $opt_name, array(
      'title'      => __( 'Breadcrumbs', 'redux-framework-demo' ),
      'id'         => 'ch_bread_switch',
      'desc'       => __( 'Activate or disactivate bradcrumbs ', 'redux-framework-demo' ),
      'subsection' => true,
      'fields'     => array(

          array(
              'id'       => 'ch_breadactive',
              'type'     => 'switch',
              'title'    => __( 'Breadcrumbs - active breadcrumbs ', 'redux-framework-demo' ),
              'subtitle' => __( '', 'redux-framework-demo' ),
              'default'  => 0,
              'on'       => 'Enabled',
              'off'      => 'Disabled',
          ),
          array(
              'id'       => 'ch_breadexclude',
              'type'     => 'select',
              'required' => array( 'ch_breadactive', '=', '1' ),
              'data' => 'post_types',
              'multi'    => true,
              'title'    => __( 'Include posts type', 'redux-framework-demo' ),
              'subtitle' => __( 'Select all posts type that will be insered in the xml sitemap', 'redux-framework-demo' ),
              'desc'     => __( '', 'redux-framework-demo' ),
              'default'  => array( 'page' ),
          ),
      )
  ) );



//***********************  Footer Settings *********************//

Redux::setSection( $opt_name, array(
        'title' => __( 'Footer', 'chooseWp_framework' ),
        'id'    => 'ch_footer_set',
        'desc'  => __( 'Footer Settings', 'chooseWp_framework' ),
        'icon'  => 'el el-picture'
    ) );

Redux::setSection( $opt_name, array(
        'title'            => __( 'Footer Settings', 'redux-framework-demo' ),
        'desc'             => __( 'Manage your footer settings: ', 'redux-framework-demo' ),
        'id'               => 'Copyright-Text',
        'subsection'       => true,
        'customizer_width' => '700px',
        'fields'           => array(
            array(
                'id'       => 'copyright',
                'type'     => 'editor',
                'title'    => __( 'Copyright', 'redux-framework-demo' ),
                'subtitle' => __( 'Insert the copyright data', 'redux-framework-demo' ),
                'desc'     => __( '', 'redux-framework-demo' ),
                'default'  => '',
                'args'   => array(
                   'teeny'            => true,
                   'textarea_rows'    => 10
                )
            ),
            array(
                'id'       => 'info_footer',
                'type'     => 'editor',
                'title'    => __( 'Info', 'redux-framework-demo' ),
                'subtitle' => __( 'Insert the info data', 'redux-framework-demo' ),
                'desc'     => __( '', 'redux-framework-demo' ),
                'default'  => '',
                'args'   => array(
        'teeny'            => true,
        'textarea_rows'    => 10
    )
            ),
            array(
                'id'       => 'ch-icon-fb',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Footer logo Icon', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( 'Add footer logo Icon', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => array( 'url' => 'http://s.wordpress.org/style/images/codeispoetry.png' ),
            ),
            array(
                'id'       => 'url_fb',
                'type'     => 'text',
                'title'    => __( 'Footer logo Url', 'redux-framework-demo' ),
                'subtitle' => __( 'Insert the footer logo url', 'redux-framework-demo' ),
                'desc'     => __( '', 'redux-framework-demo' ),
                'default'  => '#',
            ),
            array(
                'id'       => 'ch-icon-tw',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Linkedin Icon', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( 'Add Linkedin Icon', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => array( 'url' => 'http://s.wordpress.org/style/images/codeispoetry.png' ),
            ),
            array(
                'id'       => 'url_tw',
                'type'     => 'text',
                'title'    => __( 'Linkedin Url', 'redux-framework-demo' ),
                'subtitle' => __( 'Insert the linkedin url', 'redux-framework-demo' ),
                'desc'     => __( '', 'redux-framework-demo' ),
                'default'  => '#',
            ),
            array(
                'id'       => 'ch-icon-go',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Viemo Icon', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( 'Add viemo Icon', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => array( 'url' => 'http://s.wordpress.org/style/images/codeispoetry.png' ),
            ),
            array(
                'id'       => 'url_go',
                'type'     => 'text',
                'title'    => __( 'Vimeo Url', 'redux-framework-demo' ),
                'subtitle' => __( 'Insert the vimeo url', 'redux-framework-demo' ),
                'desc'     => __( '', 'redux-framework-demo' ),
                'default'  => '#',
            ),
            array(
                'id'       => 'riviera-icon-go',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Twitter Icon', 'redux-framework-demo' ),
                'compiler' => 'true',
                //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc'     => __( 'Add twitter Icon', 'redux-framework-demo' ),
                'subtitle' => __( '', 'redux-framework-demo' ),
                'default'  => array( 'url' => 'http://s.wordpress.org/style/images/codeispoetry.png' ),
            ),
            array(
                'id'       => 'url_goriviera',
                'type'     => 'text',
                'title'    => __( 'Twitter Url', 'redux-framework-demo' ),
                'subtitle' => __( 'Insert the Twitter url', 'redux-framework-demo' ),
                'desc'     => __( '', 'redux-framework-demo' ),
                'default'  => '#',
            ),
        )
    ) );




//*********************** SLIDE Settings ***********************//

Redux::setSection( $opt_name, array(
    'title' => __( 'SLIDE', 'chooseWp_framework' ),
    'id'    => 'ch_slide_set',
    'desc'  => __( 'Slide Settings', 'chooseWp_framework' ),
    'icon'  => 'el el-picture'
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Slideshow Option', 'redux-framework-demo' ),
    'id'         => 'ch_slide_switch',
    'desc'       => __( '', 'redux-framework-demo' ),
    'subsection' => true,
    'fields'     => array(

         array(
            'id'       => 'ch_slide_active_switch',
            'type'     => 'switch',
            'title'    => __( 'Enable Slideshow', 'redux-framework-demo' ),
            'subtitle' => __( 'Choose if you want to anble or disable the slideshow', 'redux-framework-demo' ),
            'default'  => 0,
            'on'       => 'Enabled',
            'off'      => 'Disabled',
        ),
        array(
            'id'       => 'ch_slide_arrows',
            'type'     => 'switch',
            'required' => array( 'ch_slide_active_switch', '=', '1' ),
            'title'    => __( 'Arrows -  Navigation arrows', 'redux-framework-demo' ),
            'subtitle' => __( 'Anable or disable navigation arrows" parent.', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => false,
        ),
        array(
            'id'       => 'ch_slide_buttons',
            'type'     => 'switch',
            'required' => array( 'ch_slide_active_switch', '=', '1' ),
            'title'    => __( 'Buttons - Dots bottom navigation', 'redux-framework-demo' ),
            'subtitle' => __( 'Anable or disable dots navigation', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => false,
        ),
        array(
            'id'       => 'ch_slide_fade',
            'type'     => 'switch',
            'required' => array( 'ch_slide_active_switch', '=', '1' ),
            'title'    => __( 'Fade transition', 'redux-framework-demo' ),
            'subtitle' => __( 'Slide transition on "Fade" or "Slide" - On next field you can set the duration of transition.', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => false,
            'on'       => 'Fade',
            'off'      => 'Slide',
        ),
        array(
            'id'            => 'ch_slide_fade_time',
            'type'          => 'slider',
            'required' => array( 'ch_slide_active_switch', '=', '1' ),
            'title'         => __( 'Set fade transition duration', 'redux-framework-demo' ),
            'subtitle'      => __( 'Have effect only if slide is in "fade" mode', 'redux-framework-demo' ),
            'desc'          => __( '' ),
            'default'       => 700,
            'min'           => 100,
            'step'          => 100,
            'max'           => 5000,
            'display_value' => 'text'
        ),
        array(
            'id'            => 'ch_slide_slide_time',
            'type'          => 'slider',
            'required' => array( 'ch_slide_active_switch', '=', '1' ),
            'title'         => __( 'Set slide transition duration', 'redux-framework-demo' ),
            'subtitle'      => __( 'Have effect only if slide is in "slide" mode', 'redux-framework-demo' ),
            'desc'          => __( '' ),
            'default'       => 700,
            'min'           => 100,
            'step'          => 100,
            'max'           => 5000,
            'display_value' => 'text'
        ),
        array(
            'id'       => 'ch_slide_autoplay',
            'type'     => 'switch',
            'required' => array( 'ch_slide_active_switch', '=', '1' ),
            'title'    => __( 'Autoplay', 'redux-framework-demo' ),
            'subtitle' => __( 'Set if slide works, in "autoplay" mode. Iy Autoplay is unset, you must anable other type of navigation "Arrows" or "Buttons" ', 'redux-framework-demo' ),
            'desc'     => __( '', 'redux-framework-demo' ),
            'default'  => false,
        ),
        array(
            'id'            => 'ch_slide_autoplay_time',
            'type'          => 'slider',
            'required' => array( 'ch_slide_active_switch', '=', '1' ),
            'title'         => __( 'Set delay time from one slide to another', 'redux-framework-demo' ),
            'subtitle'      => __( 'Have effect only if slide is in "autoplay" mode.', 'redux-framework-demo' ),
            'desc'          => __( '' ),
            'default'       => 5000,
            'min'           => 1000,
            'step'          => 500,
            'max'           => 30000,
            'display_value' => 'text'
        ),
    )
) );




/*************** Singol & Contacts Settings ***********************/

Redux::setSection( $opt_name, array(
        'title' => __( 'Contacts Settings', 'chooseWp_framework' ),
        'id'    => 'ch_contact_set',
        'desc'  => __( 'Contact Settings for Singol and Contact page', 'chooseWp_framework' ),
        'icon'  => 'el el-picture'
    ) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Contacts Option', 'chooseWp_framework' ),
    'id'         => 'ch_contacts_opt',
    'desc'       => __( '', 'chooseWp_framework' ),
    'subsection' => true,
    'fields'     => array(

         array(
            'id'       => 'contact_singol',
            'type'     => 'editor',
            'title'    => __( 'Contact', 'chooseWp_framework' ),
            'subtitle' => __( 'Insert the data to show on contact box in single page', 'chooseWp_framework' ),
            'desc'     => __( '', 'chooseWp_framework' ),
            'default'  => '',
            'args'   => array(
    'teeny'            => false,
    'textarea_rows'    => 10
    )
            ),
         )
    )
);




/*************** Setting for Production ***********************/

Redux::setSection( $opt_name, array(
    'title'      => __( 'Setting For Production', 'beprime' ),
    'id'         => 'prod_opt',
    'desc'       => __( 'Set up the following when your site is ready for production', 'beprime' ),
    'fields'     => array(
      array(
        'id'       => 'less_comp',
        'type'     => 'switch',
        'title'    => __( 'Less Compilation', 'beprime' ),
        'subtitle' => __( 'Disable when u dont need less compilation any longer', 'beprime' ),
        'desc'     => __( 'Do you need less compilation?', 'beprime' ),
        'on'       => 'Compile',
        'off'       => 'Off',
        'default'  => true,
      )
    )
  )
);




?>
