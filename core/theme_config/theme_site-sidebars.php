<?php

add_action( 'widgets_init', 'chooseWp_widgets_init' );
add_action( 'widgets_init', 'chooseWp_homewidgets_init' );

function chooseWp_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'chooseWp_framework' ),
        'id' => 'ch_sidebar_1',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget'  => '</li>',
	'before_title'  => '<h2 class="widgettitle subheader">',
	'after_title'   => '</h2>',
    ) );
}

function chooseWp_homewidgets_init() {
    register_sidebar( array(
        'name' => __( 'Home Sidebar', 'chooseWp_framework' ),
        'id' => 'ch_sidebar_2',
        'description' => __( 'Widgets in this area will be shown on homepage.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget'  => '</li>',
	'before_title'  => '<h2 class="homesidebar subheader">',
	'after_title'   => '</h2>',
    ) );
}

?>
