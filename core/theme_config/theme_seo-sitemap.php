<?php

/* ------------------------------------------------------------------------- *
* WordPress Dynamic XML Sitemap
* Codes By Emrah Gunduz & All In One SEO
* Updated And Edited By ChoosePizzi
/* ------------------------------------------------------------------------- */


if(!class_exists('Choose_SEO_sitemap'))
{
    class Choose_SEO_sitemap
    {

      private $comma_separated;
      private $isActive;

        public function __construct()
        {

          global $choose_wp;
          $this->isActive = $choose_wp['ch_seo_xmlactive'];

          if ( ($this->isActive == "1" )) {
            $xmlexclude_array = $choose_wp['ch_seo_xmlexclude'];
            $this->comma_separated = $xmlexclude_array;
            $this->ch_create_map();
          }

        } // End Construct

        function ch_create_map(){

          $def = $this->comma_separated;

         // print_r($def);

          add_action("publish_post", "ch_create_sitemap");
          add_action("publish_page", "ch_create_sitemap");

          function ch_create_sitemap() {
            $postsForSitemap = get_posts(array(
              'numberposts' => -1,
              'orderby' => 'modified',
              'data'=>'post_type',
              'post_type' => $choose_wp['ch_seo_xmlexclude'],
              'order' => 'DESC'
              ));


            $sitemap = '<?xml version="1.0" encoding="UTF-8"?>';
            $sitemap .= '<?xml-stylesheet type="text/xsl" href="' . get_stylesheet_directory_uri() . '/sitemap-style.xsl"?>';
            $sitemap .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
            foreach($postsForSitemap as $post) {
              setup_postdata($post);
              $postdate = explode(" ", $post->post_modified);
              $sitemap .= '<url>'.
              '<loc>'. get_permalink($post->ID) .'</loc>'.
              '<priority>1</priority>'.
              '<lastmod>'. $postdate[0] .'</lastmod>'.
              '<changefreq>daily</changefreq>'.
              '</url>';
            }
            $sitemap .= '</urlset>';
            $fp = fopen(ABSPATH . "sitemap.xml", 'w');
            fwrite($fp, $sitemap);
            fclose($fp);
          }
        }
        } // END class

        } // END if(!class_exists())


if(class_exists('Choose_SEO_sitemap'))
{

    // instantiate the class
    $obj = new Choose_SEO_sitemap();


}

?>
