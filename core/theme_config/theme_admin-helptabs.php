<?php

// Set the help Tabs

 $tabs = array(
        array(
            'id'      => 'choosewp-help-tab-1',
            'title'   => __( 'ChooseWp Information 1', 'redux-framework-demo' ),
            'content' => __( '<p>ChooseWp - This is the tab content, HTML is allowed.</p>', 'chooseWp_framework' )
        ),
        array(
            'id'      => 'choosewp-help-tab-2',
            'title'   => __( 'ChooseWp Information 2', 'redux-framework-demo' ),
            'content' => __( '<p>ChooseWp - This is the tab content, HTML is allowed.</p>', 'chooseWp_framework' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'chooseWp_framework' );
    Redux::setHelpSidebar( $opt_name, $content );

?>