<?php

/* ------------------------------------------------------------------------- *
* WordPress Custom Post Type
*
* By ChoosePizzi
/* ------------------------------------------------------------------------- */


if(!class_exists('Choose_cpt'))
{
  class Choose_cpt
  {

    public function __construct()
    {
      add_action('init',array(&$this,'news_posttype'));
      add_filter( 'pre_get_posts',array(&$this,'namespace_add_custom_types') );
    }

    function namespace_add_custom_types( $querycpt ) {
      // if($querycpt->is_main_query() && ( is_category() || is_tag() )) {
      //   $querycpt->set( 'post_type', array('post','news') );
      // }
    }




    /****************************/
    /*   Copy Modify & Past     */
    /***************************/

    function news_posttype() {
      register_post_type('news', array(
        'label' => __('News', 'choose_wp' ),
        'description' => '',
        'menu_icon'=> 'dashicons-pressthis',   
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'capability_type' => 'post',
        'hierarchical' => true,
        'has_archive' => true,
        'rewrite' => array(
          'slug'  => 'News',
          'with_front'  => false,
          ),
        'query_var' => true,
        'menu_position' => 20,
        'supports' => array(
          'title',
          'editor',
          'thumbnail'
        )
      ));
    }
  }
}


if(class_exists('Choose_cpt'))
{
    // instantiate the class
    $obj = new Choose_cpt();
}

?>
