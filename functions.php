<?php
/**
 * THEME MAIN FUNCTIONS FILE
 *
 * Version 1.2.0
 */



//********************* Activate chooseWp-framework ****************/

if ( !class_exists( 'beprimeWp-framework' ) && file_exists( dirname( __FILE__ ) . '/core/ReduxCore/framework.php' ) ) {
    require_once( dirname( __FILE__ ) . '/core/ReduxCore/framework.php' );
}
if ( !isset( $choose_redux ) && file_exists( dirname( __FILE__ ) . '/core/framework-config.php' ) ) {
    require_once( dirname( __FILE__ ) . '/core/framework-config.php' );
}



//********************* Compile less ****************/

global $choose_wp;

$LessActive = $choose_wp['less_comp'];

if ( ($LessActive == "1" ) ) {
  require_once( dirname( __FILE__ ) . '/core/less/lessc.inc.php' );

  $less = new lessc;

  //Compressed Output
  $less->setFormatter("compressed");

  try {
    $less->compileFile(__DIR__."/less/default.less", __DIR__.'/css/default.css');
  }
  catch (exception $e) {
    echo '<h1>Error making CSS files: ' . $e->getMessage() .'</h1>';
  }
}



//********************* Add theme Support ****************/

add_theme_support( 'post-thumbnails' );

/**
 * CUSTOM THUMBNAIL sizes
 *
 * add_image_size( 'category-thumb', 300 ); // 300 pixels wide (and unlimited height)
 * add_image_size( 'homepage-thumb', 220, 180, true ); // (cropped)
 */

function beprime_theme_setup() {
  add_image_size( 'size-1920', 1920 );
}
add_action( 'after_setup_theme', 'beprime_theme_setup' );




//********************* Includes Plugin - ACF Pro , WPML, ecc ****************/

 require_once( dirname( __FILE__ ) . '/core/theme_config/theme_admin-plugins.php' );

//********************* Create & Register Website Navigations ****************/

 require_once( dirname( __FILE__ ) . '/core/theme_config/theme_site-nav.php' );

//********************* Include scrpts & js ****************/

 require_once( dirname( __FILE__ ) . '/core/theme_config/theme_site-scripts.php' );

//********************* Compatibility for bootstrap classes on WP nav menus ****************/

 require_once( dirname( __FILE__ ) . '/core/theme_config/wp_bootstrap_navwalker.php' );

//********************* SEO XML Sitemap & js ****************/

 require_once( dirname( __FILE__ ) . '/core/theme_config/theme_seo-sitemap.php' );

//********************* Register Custom Post Type ****************/

 //require_once( dirname( __FILE__ ) . '/core/theme_config/theme_cpt.php' );

//********************* SideBars ****************/

 //require_once( dirname( __FILE__ ) . '/core/theme_config/theme_site-sidebars.php' );

//********************* Breadcrumbs ****************/

  //require_once( dirname( __FILE__ ) . '/core/theme_config/theme_admin-breadcrumbs.php' );

  //global $choose_wp;
  // Print Breadcrumps
/*function choose_breadcrumbs(){
    $isActiveBread = $choose_wp['ch_breadactive']; if ( ($isActiveBread == "1" )) {

			global $post;
			$postTp = get_post_type( $post );
			$excludePage = $choose_wp['ch_breadexclude'];

			foreach ($excludePage as $value) {
				if( $value == $postTp){
					choose_create_breadcrumbs();
				}
			}
		}
}*/
