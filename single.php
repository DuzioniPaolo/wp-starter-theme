<?php get_header(); ?>
<div class="container">
<div class="row">
	<div class="col-md-12">


	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

	<div class="entry" id="post-<?php the_ID(); ?>">
		<header>
			<h1><?php the_title(); ?></h1>
			<div class="meta panel">
				<div class="row">
					<div class="col-md-2">
						<h6 class="subheader"><em> <i class="fa fa-calendar"></i> </em> <?php the_time('F jS, Y') ?> <em> <i class="fa fa-user"></i></em> <?php the_author() ?></h6>
					</div>
					<div class="col-md-10">
						<a href="#" class="button right split tiny comments_btn"><i class="fa fa-comments"></i> <span data-dropdown="drop<?php echo $number ?>"></span></a><br>
						<ul id="drop<?php echo $number ?>" class="f-dropdown" data-dropdown-content>
								<li><p><?php comments_number( 'no responses', 'one response', '% responses' ); ?></p></li>
						</ul>
					</div>
				</div>
			</div>
		</header> <!-- End Header -->

		<div class="entry">
      <?php the_content(); ?>
		</div> <!-- End Entry -->

		<section class="share_nav">
			<div class="icon-bar six-up">
				<a href="https://plus.google.com/share?url=[<?php the_permalink(); ?>]" class="item">
					<i class="fa fa-google-plus"></i>
				</a>
				<a href="http://www.facebook.com/share.php?u=['']&title=[<?php the_title(); ?>]" class="item">
					<i class="fa fa-facebook"></i>
			    </a>
				<a class="item">
					<i class="fa fa-linkedin"></i>
				</a>
				<a class="item">
					<i class="fa fa-twitter"></i>
				</a>
				<a class="item">
					<i class="fa fa-envelope-o"></i>
				</a>
				<a class="item">
					<i class="fa fa-print"></i>
				</a>
			</div>
		</section>
		<!-- Ens Share -->

	</div> <!-- End Entry -->



	</div>
		</div> <!-- End Post Wrp -->
		<div class="large-9 columns">
			<div class="large-12 columns categories_nav">
					<span class="ch_tooltip ch_tooltip-top" data-tooltip="<?php echo __( 'Categories', 'chooseWp_framework' ); ?>"><i class="fa fa-folder-open-o"></i></span>
						 <?php the_category(' '); ?>
			</div>

			<div class="large-12 comment_wrp">
				<section class="commentlist">
				<?php comments_template(); ?>
				</section>
				</div>
		</div>
</div>


	<?php endwhile; endif; ?>

</div>
</div>
</div>

<?php get_footer(); ?>
