# WP Starter Theme

*Version 1.3.4*

A fast starter WordPress theme for speed up your next project.

### Includes
- Redux Framework
- Less Autocompiler (Less.php)
- Bootstrap with Nav Walker for WordPress

### Utilities
- Font Awesome
- Slider Pro
- TosRus Lightbox
