<?php get_header(); ?>

<div class="container">
<div class="row">

	<div class="col-md-3">
		<?php get_sidebar(); ?>
	</div>

	<div class="large-9 columns posts_wrp">
		<?php // choose_breadcrumbs(); ?>
		<?php if (have_posts()) : $number = 0;
		    while (have_posts()) : the_post(); ?>

					<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
						<header>
							<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
							<div class="meta panel">
								<h6 class="subheader"><em> <i class="fa fa-calendar"></i> </em> <?php the_time('F jS, Y') ?> <em> <i class="fa fa-user"></i></em> <?php the_author() ?></h6>
							</div>
						</header>

						<?php if ( has_post_thumbnail() ) : ?>
							<?php the_post_thumbnail(); ?>
						<?php endif; ?>
						<div class="entry">
							<?php the_excerpt(); ?>
						</div>

						<footer>
							<div>
									<i class="fa fa-folder-open-o"></i> <?php the_category(', '); ?>
							</div>
						</footer>
					</article>

			<?php endwhile; ?>
		<?php else : ?>

			<div>
	 			<p><?php echo __( 'No posts found.', 'beprime' ); ?></p>
	  		<a href="#" class="close"><i class="fa fa-search"></i></a>
			</div>

		<?php endif; ?>
	</div>

</div>
</div>

<?php get_footer(); ?>
