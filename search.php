<?php if(__FILE__ == $_SERVER['SCRIPT_FILENAME']){ die(); } ?>

<?php get_header(); global $wp_query; $total_results = $wp_query->found_posts; ?>


<div class="container">
  <div class="row">
		<div class="cont-int clearfix">


			<section id="page_search" class="listarticleblog col-md-12">
				<header id="titlehead">
					<h1 id="pagetitle">
						<span><?php echo sprintf(__('Search for "%s"', 'beprime'), esc_html(get_search_query()));?></span>
					</h1>
				</header>
				<span id="total-results">
					<?php printf(__('Found %1$s results', 'beprime'), $total_results); ?>
				</span>
				<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
					<a class="result-entry" href="<?php the_permalink(); ?>">
						<h3 <?php if(get_schema_post() == TRUE){ ?>class="entry-title"<?php } ?>><?php the_title(); ?></h3>
						<div class="textpost <?php if(get_schema_post() == TRUE){ ?>entry-content<?php } ?>">
							<?php if(get_the_content() != ""){ ?>
							<?php the_excerpt(); ?></p>
							<?php } ?>
						</div>
					</a>
				<?php endwhile;
        else : ?>
					<article class="no-resutls-found">
						<h4><?php _e('No results found. Please try with different terms. ', 'beprime'); ?></h4>
					</article>
				<?php endif; ?>
			</section>


		</div>
	</div>
</div>



<?php get_footer(); ?>
