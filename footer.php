<?php global $choose_wp; ?>

<footer id="footer" role="contentinfo" class="clearfix">
	 <div class="footer_main">
		<div class="row">


			<!-- <div class="large-2 columns social_icons">
				<div class="social_conts columns">
					<a href="<?php// echo $choose_wp['url_fb']; ?>" class="fb_icon" target="_blank"><img alt="Royal Facebook" src="<?php// echo $choose_wp['ch-icon-fb']['url']; ?>"></a>
				</div>
			</div> -->

			<!-- <div class="large-7 columns footer_info"> <?php //echo __( $choose_wp['info_footer'], 'chooseWp_framework' ); ?> </div> -->
			<!-- <div class="large-3 columns footer_menu">
				<a href="<?php //echo $choose_wp['url_tw']; ?>" class="fb_icon" target="_blank"><img alt="Royal Twitter" src="<?php //echo $choose_wp['ch-icon-tw']['url']; ?>"></a>
				<a href="<?php //echo $choose_wp['url_go']; ?>" class="fb_icon" target="_blank"><img alt="Royal Blog" src="<?php //echo $choose_wp['ch-icon-go']['url']; ?>"></a>

				<a href="<?php //echo $choose_wp['url_goriviera']; ?>" class="fb_icon" target="_blank"><img alt="Royal Home" src="<?php //echo $choose_wp['riviera-icon-go']['url']; ?>"></a>
			</div> -->


		</div>
	</div>

	<div class="copy_right">
		<div class="container">
				<div class="col-md-12 text-center">
					<div id="copyright">
						<?php echo $choose_wp['copyright']; ?>
					</div>
				</div>
		</div>
	</div>
</footer>


<?php wp_footer(); ?>
</body>
</html>
